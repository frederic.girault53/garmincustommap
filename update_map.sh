#!/bin/bash
echo "Update of the map $1 ...."

land=$1
id=$2
type=$3
url=$4

land_lower=$(echo $land | tr '[:upper:]' '[:lower:]')
land_lower=$(echo $land_lower | tr ' ' _ )
file="$land_lower".osm.pbf


cd "carte_$land_lower"

curl -L -o $file $url

cd ..

bash create_map.sh "$land" $id $type


