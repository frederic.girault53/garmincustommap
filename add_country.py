#!/usr/bin/python
import sys
import os
import subprocess
from scripts_hgt.get_hgt import get_hgt
from scripts_hgt.hgt_to_osm import hgt_to_osm
from get_contours import get_contours
import os, shutil, fnmatch
import subprocess
import time

country_name=sys.argv[1]
type=sys.argv[2]
url=sys.argv[3]
 
start_time = time.perf_counter()
country_name_lower_case = country_name.lower().replace(" ", "_")
country_name_upper_case = country_name_lower_case.capitalize()


os.makedirs("dem/"+country_name_lower_case, exist_ok=True)

os.makedirs("carte_"+country_name_lower_case, exist_ok=True)

#File id
file_in = open("id.md", "rt")

lines = file_in.readlines()
if (len(lines) >1):
    last_line=lines[-1]
    result = last_line.find(' => ')
    id="{:02d}".format(int(last_line[result+4:len(last_line)])+1)
else:
    id="00"

file_in = open("id.md", "rt")

file_source = file_in.read()
file_modif = file_source+'\n- '+country_name+' => '+id
file_in.close()

file_out = open("id.md", "wt")
file_out.write(file_modif)
file_out.close()
 
#File update all

file_in = open("update_all.sh", "rt")


file_source = file_in.read()
file_modif = file_source.replace('#end_file', 'bash update_map.sh \''+country_name+'\' '+id+ ' '+type+' '+url+'\ntime_task \''+country_name+'\'\n\n#end_file')
file_in.close()

file_out = open("update_all.sh", "wt")
file_out.write(file_modif)
file_out.close()

#Get contours
get_contours(country_name, url)

#Launch script
subprocess.run(["bash", "update_map.sh",country_name,id,type,url])
stop_time = time.perf_counter()

print("Add country in "+time.strftime('%H:%M:%S', time.gmtime(stop_time - start_time)))
