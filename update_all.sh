#!/bin/bash

rm update.log

time_task () {
   if [ "$1" == "total" ];then
      start_time=start_time_full
   fi   
   T="$(($(date +%s)-start_time))"
   D=$((T/60/60/24))
   H=$((T/60/60%24))
   M=$((T/60%60))
   S=$((T%60))
   time_display=""
   if [ $D -gt 0 ];then
      time_display="${time_display}${D} days "
   fi
   if [ $H -gt 0 ];then
      time_display="${time_display}${H} hours "
   fi
   if [ $M -gt 0 ];then
      time_display="${time_display}${M} minutes "
   fi
   time_display="${time_display}${S} seconds "

   echo "$1 finished in ${time_display}">>update.log
   start_time="$(date +%s)"
}

start_time_full="$(date +%s)"
start_time="$(date +%s)"

bash update_map.sh 'France' 00 rando http://download.geofabrik.de/europe/france-latest.osm.pbf
time_task 'France'

bash update_map.sh 'Reunion' 02 rando http://download.geofabrik.de/europe/france/reunion-latest.osm.pbf
time_task 'Reunion'

bash update_map.sh 'Belgique' 03 rando http://download.geofabrik.de/europe/belgium-latest.osm.pbf
time_task 'Belgique'

bash update_map.sh 'Suisse' 04 rando http://download.geofabrik.de/europe/switzerland-latest.osm.pbf
time_task 'Suisse'

bash update_map.sh 'Polynesie Francaise' 08 rando http://download.geofabrik.de/australia-oceania/polynesie-francaise-latest.osm.pbf
time_task 'Polynesie Francaise'

bash update_map.sh 'Espagne' 09 rando http://download.geofabrik.de/europe/spain-latest.osm.pbf
time_task 'Espagne'

bash update_map.sh 'Italie' 10 rando http://download.geofabrik.de/europe/italy-latest.osm.pbf
time_task 'Italie'

bash update_map.sh 'Andorre' 11 rando http://download.geofabrik.de/europe/andorra-latest.osm.pbf
time_task 'Andorre'

bash update_map.sh 'Seychelles' 12 rando http://download.geofabrik.de/africa/seychelles-latest.osm.pbf
time_task 'Seychelles'

bash update_map.sh 'Allemagne' 13 rando http://download.geofabrik.de/europe/germany-latest.osm.pbf
time_task 'Allemagne'

bash update_map.sh 'Iles Canaries' 14 rando http://download.geofabrik.de/africa/canary-islands-latest.osm.pbf
time_task 'Iles Canaries'

bash update_map.sh 'Martinique' 15 rando http://download.geofabrik.de/europe/france/martinique-latest.osm.pbf
time_task 'Martinique'

bash update_map.sh 'Portugal' 16 rando http://download.geofabrik.de/europe/portugal-latest.osm.pbf
time_task 'Portugal'

bash update_map.sh 'Guadeloupe' 17 rando http://download.geofabrik.de/europe/france/guadeloupe-latest.osm.pbf
time_task 'Guadeloupe'

bash update_map.sh 'Pays Bas' 18 rando http://download.geofabrik.de/europe/netherlands-latest.osm.pbf
time_task 'Pays Bas'

bash update_map.sh 'Croatie' 19 rando http://download.geofabrik.de/europe/croatia-latest.osm.pbf
time_task 'Croatie'

bash update_map.sh 'Autriche' 20 rando http://download.geofabrik.de/europe/austria-latest.osm.pbf
time_task 'Autriche'

bash update_map.sh 'Maurice' 21 rando http://download.geofabrik.de/africa/mauritius-latest.osm.pbf
time_task 'Maurice'

bash update_map.sh 'Quebec' 22 rando http://download.geofabrik.de/north-america/canada/quebec-latest.osm.pbf
time_task 'Quebec'

bash update_map.sh 'Nouveau Brunswick' 23 rando http://download.geofabrik.de/north-america/canada/new-brunswick-latest.osm.pbf
time_task 'Nouveau Brunswick'

bash update_map.sh 'Ontario' 24 rando http://download.geofabrik.de/north-america/canada/ontario-latest.osm.pbf
time_task 'Ontario'

bash update_map.sh 'Maine' 25 rando http://download.geofabrik.de/north-america/us/maine-latest.osm.pbf
time_task 'Maine'

bash update_map.sh 'Luxembourg' 26 rando http://download.geofabrik.de/europe/luxembourg-latest.osm.pbf
time_task 'Luxembourg'

bash update_map.sh 'Islande' 27 rando http://download.geofabrik.de/europe/iceland-latest.osm.pbf
time_task 'Islande'

bash update_map.sh 'Pologne' 28 rando http://download.geofabrik.de/europe/poland-latest.osm.pbf
time_task 'Pologne'

bash update_map.sh 'Guyane' 29 rando http://download.geofabrik.de/europe/france/guyane-latest.osm.pbf
time_task 'Guyane'

bash update_map.sh 'Ecosse' 30 rando http://download.geofabrik.de/europe/great-britain/scotland-latest.osm.pbf
time_task 'Ecosse'

bash update_map.sh 'Afrique du Sud' 31 rando http://download.geofabrik.de/africa/south-africa-latest.osm.pbf
time_task 'Afrique du Sud'

bash update_map.sh 'Nouvelle Calédonie' 32 rando http://download.geofabrik.de/australia-oceania/new-caledonia-latest.osm.pbf
time_task 'Nouvelle Calédonie'

bash update_map.sh 'Californie' 33 rando http://download.geofabrik.de/north-america/us/california-latest.osm.pbf
time_task 'Californie'

bash update_map.sh 'Nevada' 34 rando http://download.geofabrik.de/north-america/us/nevada-latest.osm.pbf
time_task 'Nevada'

bash update_map.sh 'Utah' 35 rando http://download.geofabrik.de/north-america/us/utah-latest.osm.pbf
time_task 'Utah'

bash update_map.sh 'Arizona' 36 rando http://download.geofabrik.de/north-america/us/arizona-latest.osm.pbf
time_task 'Arizona'

bash update_map.sh 'Hawaii' 37 rando http://download.geofabrik.de/north-america/us/hawaii-latest.osm.pbf
time_task 'Hawaii'

bash update_map.sh 'Colombie-Britannique' 38 rando http://download.geofabrik.de/north-america/canada/british-columbia-latest.osm.pbf
time_task 'Colombie-Britannique'

bash update_map.sh 'Alberta' 39 rando http://download.geofabrik.de/north-america/canada/alberta-latest.osm.pbf
time_task 'Alberta'

bash update_map.sh 'Nepal' 40 rando http://download.geofabrik.de/asia/nepal-latest.osm.pbf
time_task 'Nepal'

#end_file

time_task total 


