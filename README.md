# Création d'une carte Garmin personnalisé 

Dans les lignes qui suit j'explique pour une carte de la france.

## Téléchargement des outils
```bash
bash download_require.sh
```

## Téléchargement des fichiers pour les courbes

https://search.earthdata.nasa.gov/search

Vous allez récupérer les fichiers en format tif il faudra le changer en hgt puis en OSM
```
gdal_translate -of SRTMHGT mon_fichier.tif mon_fichier.hgt
```
Sinon pour l'europe vous avez ce site qui permet de récupérer les fichiers en hgt directement.

http://data.opendataportal.at/dataset/dtm-europe

Le lien pour la france est ici:

https://drive.google.com/drive/folders/18019OJyys9meLMzJikglC3j7ctKX2x3S

Pour passer du format hgt au format OSM nous allons utiliser le logiciel hgt2osm que vous pouvez télécharger ici:
https://github.com/FSofTlpz/Hgt2Osm2/tree/master/bin

La commande est la suivante:
```
hgt2osm.exe --HgtPath=. --WriteElevationType=false --FakeDistance=-0.5 --MinVerticePoints=3 --MinBoundingbox=0.00016 --DouglasPeucker=0.05 --MinorDistance=10 --OutputOverwrite=true
```

Maintenant que les courbes sont aux formats OSM je vous recommande de les sauvegarder car cette étape ne sera rarement refaite vu que les fichies de courbes ne changent presque jamais.

## Téléchargement de la carte
Sur le site http://download.geofabrik.de/ vous pouvez récupérer la région que vous souhaitez réaliser.

Pour la france le lien est : http://download.geofabrik.de/europe/france-latest.osm.pbf

## Découper les fichiers
Nous allons utiliser l'application splitter

Pour les courbes comme c'est au format OSM vous pouvez lancer la commande suivante:

```bash
java -Xmx32768m -jar splitter.jar --mapid=73240100 --max-nodes=1600000 --keep-complete=false *.OSM

mv template.args courbes.args
```

Pour découper le fichier de la france vous pouvez lancer la commande suivante:

```bash
java -Xmx32768m -jar splitter.jar --mapid=63240101 --max-nodes=1000000 --keep-complete=true --route-rel-values=foot,hiking --overlap=0 france-latest.osm.pbf

mv template.args france-latest.args
```

## Générer le fichier IMG pour votre montre
Vous devez récuper le répertoire style qui se trouve sur ce github, il contient le fichier TYP et les style pour mkgmap.

```bash
java -Xmx32768m -jar ../mkgmap-r4802/mkgmap.jar --road-name-pois --add-pois-to-areas --add-pois-to-lines --remove-short-arcs --precomp-sea=../sea.zip --x-check-precomp-sea=0  --style-file=../style/rando ../style/rando.TYP --family-name="Test" --description="Test" --mapname=94240105 --family-id=1 --product-id=1 --latin1 --net --route --road-name-pois --gmapsupp -c france-latest.args -c courbes.args
```

Il ne vous reste plus qu'a copier le fichier gmapsupp.img dans montre appareil Garmin

> Attention il est possible qu'un fichier se porte déja ce nom, si c'est le cas renommer votre fichier autrement cela n'a pas d'importance. 

## Pour aller plus loin
Comme je gère plusieurs régions j'ai donc réaliser des scripts car je ne connais pas les commandes sur le bout des doigts.

### Environnement
Mes scripts sont testés sur MacOsX et Ubuntu

Pour télécharger les outils et commande nécessaire 
```bash
bash download_require.sh
```
> Sous Mac OS X il faut installer les commandes avec votre outils préferé, il vous faut ``curl``, ``python3``, ``java``, ``unzip``

### Ajouter une région
Cette commande permet de lancer toutes les commandes à la suite pour vous éviter de le faire manuellement.

A la suite de cette commande vous allez récupérer un fichier IMG pour votre montre et votre région sera rajouté dans le script `update_all.sh` pour de future mise à jour.

```python
python add_country.py NOM_DE_LA_REGION TYPE URL_OSM_GEOFABRIK
```
- NOM_DE_LA_REGION : Nom de la carte dans la montre mais aussi pour les répertoires.
- TYPE : rando ou route, le style rando est celui que je partage sur le site mais un autre style existe pour le vélo de route 
- URL_OSM_GEOFABRIK :  url du fichier de votre région sur le site http://download.geofabrik.de


exemple pour ajouter la France pour le type rando:
```python
python add_country.py France rando http://download.geofabrik.de/europe/france-latest.osm.pbf
```
### Mettre à jour mes cartes
Rien de plus simple un script est modifier lorsque vous rajoutez une région pour contenir les commandes pour mettre à jour vos cartes.

```bash
bash update_all.sh
```

## Détails des scripts
Si vous avez besoin vous pouvez executer les différents script manuellement un part un, je ne recommande pas cette méthode mais si il y a des erreurs ca permet de relancer que celle qui échoue.

### `download_require.sh`
Le script `download_require.sh` permet de télécharger les logiciels splitter et mkgmap ainsi que sous Linux les commandes nécessaires au bon fonctionnement des scripts. Il execute aussi la commande `pip install -r requirements.txt` pour télécharger les librairies python.
> Attention, il est possible que les versions ne mkgmap ou/et splitter doivent être mise à jour pour que leurs téléchargement.
```bash
bash download_require.sh
```

### `get_contours.py`
Le script `get_contours.py` permet de télécharger les fichiers hgt contenu sur le site https://urs.earthdata.nasa.gov/ et de les convertirs au format osm pour être utilisé par les autres scripts. 
```python
python get_contours.py NOM_DE_LA_REGION URL_OSM_GEOFABRIK
```
- NOM_DE_LA_REGION : Nom de la carte dans la montre mais aussi pour les répertoires.
- URL_OSM_GEOFABRIK :  url du fichier de votre région sur le site http://download.geofabrik.de
Pour notre exemple:
```bash
python get_contours.py France http://download.geofabrik.de/europe/france-latest.osm.pbf
```
 
### `update_map.sh`
Le script `update_map.sh` permet de télécharger le fichier osm et de lancer le script `create_map.sh`.

Ce script considère que le répertoire carte_NOM_DE_LA_REGION existe, dans notre exemple carte_france

```bash
bash update_map.sh NOM_DE_LA_REGION ID TYPE URL_OSM_GEOFABRIK
```
- NOM_DE_LA_REGION : Nom de la carte dans la montre mais aussi pour les répertoires.
- ID :  Identifiant de la carte sur la montre, celui-ci doit être unique entre toute les cartes sinon il risque d'avoir des cartes non disponible sur la montre. Ne connaissant pas les identifiants des autres il est possible que vous devriez modifier si vous utilisez le même qu'une carte que vous avez deja sur votre montre.
- TYPE : rando ou route, le style rando est celui que je partage sur le site mais un autre style existe pour le vélo de route 
- URL_OSM_GEOFABRIK :  url du fichier de votre région sur le site http://download.geofabrik.de

Pour notre exemple:
```bash
bash update_map.sh France 00 rando http://download.geofabrik.de/europe/france-latest.osm.pbf
```
### `create_map.sh`
Le script `create_map.sh` permet de créer un fichier pour votre appareil Garmin.

Ce script considère que le répertoire carte_NOM_DE_LA_REGION existe dans notre exemple carte_france

En fonction de vos besoins si vous souhaitez les courbes les fichiers osm des courbes doivent être présent dans le répertoire carte_NOM_DE_LA_REGION si ils ne sont pas présent la génération se fera sans l'intégration des courbes.

```bash
bash create_map.sh NOM_DE_LA_REGION ID TYPE
```
- NOM_DE_LA_REGION : Nom de la carte dans la montre mais aussi pour les répertoires.
- ID :  Identifiant de la carte sur la montre, celui-ci doit être unique entre toute les cartes sinon il risque d'avoir des cartes non disponible sur la montre. Ne connaissant pas les identifiants des autres il est possible que vous devriez modifier si vous utilisez le même qu'une carte que vous avez deja sur votre montre.
- TYPE : rando ou route, le style rando est celui que je partage sur le site mais un autre style existe pour le vélo de route 

Pour notre exemple:
```bash
bash create_map.sh France 00 rando
```
>Attention à la fin du script il déplace le fichier réaliser dans ~/Documents/Mega/Open_Garmin_Map/ cela est utile pour moi mais peut être pas pour vous. Modifier pour vos besoins si besoin.

>La RAM disponible pour java est indiqué à 32768m (32go) en fonction de votre ordinateur modifié cette valeur.

## Si vous voulez personnaliser la carte

Doc pour le style:

https://www.mkgmap.org.uk/doc/pdf/style-manual.pdf

TYPViewer pour éditer le fichier TYP:

https://sites.google.com/site/sherco40/

