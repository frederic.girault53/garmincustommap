#!/usr/bin/python
import sys
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.geometry import LineString
from shapely.ops import split as splitShapely

def split_file (file,count):
    file_in = open(file, "rt")
    polygon = file_in.read()
    point_list=list()
    for line in polygon.splitlines():
        if(line!='none' and line!='1' and line!='END'):
            split = line.split("   ")
            longitude_string = split[1]
            latitude_string = split[2]
            
            point_list.append(Point(float(longitude_string),float(latitude_string)))
    polygon = Polygon(point_list)
    polygon_list=list()
    polygon_list.append(polygon)
    while len(polygon_list)<count:
        polygon_list_new=list()
        for item in polygon_list:   
            result = splitPolygon(item)
            polygon_list_new.append(result.geoms[0])
            polygon_list_new.append(result.geoms[1])
        polygon_list=polygon_list_new

    for idx, item in enumerate(polygon_list):
        txt="none\n1"
        for coord in item.exterior.coords:
            txt=txt+"\n    "+"{:e}".format(coord[0])+"    "+"{:e}".format(coord[1])
        txt=txt+"\nEND\nEND"
        file_out = open(file.split(".")[0]+"_"+str(idx+1)+".poly", "wt")
        file_out.write(txt)
        file_out.close() 

def splitPolygon(polygon):
    x= polygon.bounds[0]
    tolerance=0.1
    area_target=polygon.area/2
    area_tolerance_min= area_target-area_target*tolerance  
    area_tolerance_max= area_target+area_target*tolerance
    line = LineString([(x, polygon.bounds[1]), (x, polygon.bounds[2])])
    area=0
    while area>area_tolerance_max or area<area_tolerance_min and x<polygon.bounds[2]:
        result = splitShapely(polygon, line)      
        area = result.geoms[0].area
        x+=1
        line = LineString([(x, polygon.bounds[1]), (x, polygon.bounds[2])])
    return splitShapely(polygon, line)      
    
if __name__ == '__main__':
    file=sys.argv[1]
    count=int(sys.argv[2])
    split_file(file, count) 